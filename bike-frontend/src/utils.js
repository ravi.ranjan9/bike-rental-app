export const Util = {
    sleep: (millis) => new Promise((resolve) => setTimeout(resolve, millis))
};

export const URL = `https://bike-rental-7.herokuapp.com`;

export const jwtSecret = 'askd364egrg734te374terg';

export const orangeColor = '#f0dc82';